//
//  ViewController.swift
//  DPHTextValidatorsProject
//
//  Created by Vitalii Todorovych on 2/13/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addValidators()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addValidators() {
        emailTextField.addValidators([DPHEmailValidator()])
    }

    @IBAction func validateAction(sender: AnyObject) {
        if (emailTextField.isValidData()) {
            self.showAlert("Success");
        }else{
            self.showAlert("Error");
        }
        
        
    }
    
    func showAlert(message : String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

