//
//  DPHValidatorObject.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 12.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation

protocol DPHValidatorProtocol {
    
    func validateValue(value: AnyObject!, inspectedObjectName: String?) -> Bool
    var errorMessage : String {get}
}

class DPHValidatorObject: DPHValidatorProtocol {
    
    private var errorMessageString : String! = "Validate error"
    var inspectedObjectName : String?
    
    func validateValue(value: AnyObject!, inspectedObjectName: String?) -> Bool {
        if (inspectedObjectName != nil) {
            self.inspectedObjectName = inspectedObjectName
            self.updateErrorMessage("Validate error in \(self.inspectedObjectName!)")
        }
        return true
    }
    
    func updateErrorMessage(text: String!) {
        self.errorMessageString = text
    }
    
    var errorMessage : String {
        return self.errorMessageString
    }
}