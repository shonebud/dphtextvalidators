//
//  DPHRequiredValueValidator.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 13.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation

class DPHRequiredValueValidator: DPHValidatorObject{
    
    var minCountValue : Int?
    var maxCountValue : Int?
    
    // MARK: Init
    override init() {
        super.init()
    }
    
    init(minCountValue: Int?, max: Int?) {
        super.init()
        self.minCountValue = minCountValue
        self.maxCountValue = max
    }
    
    // MARK: Override
    override func validateValue(value: AnyObject!, inspectedObjectName: String?) -> Bool {
        super.validateValue(value, inspectedObjectName: inspectedObjectName)
        
        if var stringValue = value as? String  {
            stringValue = stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            if (self.minCountValue != nil) && (stringValue.characters.count < self.minCountValue) {
                self.updateErrorMessage(String(format: NSLocalizedString("min_characters_error_message", comment: ""), self.inspectedObjectName ?? "", self.minCountValue!))
                return false
            }
            if (self.maxCountValue != nil) && (stringValue.characters.count > self.maxCountValue) {
                self.updateErrorMessage(String(format: NSLocalizedString(NSLocalizedString("max_characters_error_message", comment: ""), comment: ""), self.inspectedObjectName ?? "", self.maxCountValue!))
                return false
            }
            if stringValue.characters.count <= 0{
                self.updateErrorMessage(String().stringByAppendingFormat(NSLocalizedString("empty_lield_error", comment: ""),(self.inspectedObjectName) ?? ""))
                return false
            }
            
            return true
        }else{
            return false
        }
        
    }
}