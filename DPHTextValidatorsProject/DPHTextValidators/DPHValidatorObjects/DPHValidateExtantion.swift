//
//  DPHValidateExtantion.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 26.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation
import UIKit

typealias InspectableObjectDoneBlockType = (success: Bool, inspectedObjects: DPHValidateExtantionProtocol?, wrongValidatorObjects: DPHValidatorProtocol?) -> Void
typealias InspectableManyObjectDoneBlockType = (success: Bool, inspectedObjects: [DPHValidateExtantionProtocol]?, wrongValidatorObjects: [DPHValidatorProtocol]?) -> Void

protocol DPHValidateExtantionProtocol: AnyObject {
    
    func valueForValidating() -> AnyObject!
    func inspectedObjectName() -> String?
    
    func addValidators(validators: [DPHValidatorProtocol])
    func isValidData() -> Bool
    func isValidData(inspectableDoneBlock: InspectableObjectDoneBlockType?)
    
    func showErrorMessage(flag: Bool)
    func displayErrorMesage(message: String)
}

class  DPHValidateExtantion {
    
    var owner            : DPHValidateExtantionProtocol!
    var showErrorMessage : Bool!                    = false
    var validators       : [DPHValidatorProtocol]? = nil
    
    // MARK: Init
    init(owner: DPHValidateExtantionProtocol!) {
        self.owner = owner
    }
    
    
    func addValidators(validators: [DPHValidatorProtocol]) {
        if self.validators == nil {
            self.validators = [DPHValidatorProtocol]()
        }
        self.validators! += validators
    }
    
    
    func isValidData(inspectableDoneBlock: InspectableObjectDoneBlockType?){
        
        if let validators = self.validators {
            for validatorObject in validators {
                //If validateValue is failed
                if !validatorObject.validateValue(self.owner.valueForValidating(), inspectedObjectName: self.owner.inspectedObjectName()) {
                    //Show error message if need
                    if (self.showErrorMessage == true) {
                        self.displayErrorMesage(validatorObject.errorMessage)
                    }
                    if let inspectableDoneBlock = inspectableDoneBlock {
                        inspectableDoneBlock(success: false, inspectedObjects: self.owner, wrongValidatorObjects: validatorObject)
                    }
                    return
                }
            }
        }
        if let inspectableDoneBlock = inspectableDoneBlock {
            inspectableDoneBlock(success: true, inspectedObjects: nil, wrongValidatorObjects: nil)
        }
    }
    
    func isValidData() -> Bool {
        
        if let validators = self.validators {
            for validatorObject in validators {
                //If validateValue is failed
                if !validatorObject.validateValue(self.owner.valueForValidating(), inspectedObjectName: self.owner.inspectedObjectName()) {
                    //Show error message if need
                    if (self.showErrorMessage == true) {
                        self.displayErrorMesage(validatorObject.errorMessage)
                    }
                    return false
                }
            }
        }
        return true
    }
    
    func displayErrorMesage(message: String) {
//        Noise.showError(message: message)
    }
    
}

// EXM: [textField <DPHValidateExtantionProtocol>, textView <DPHValidateExtantionProtocol>].isValidObjects()
extension CollectionType where Generator.Element: DPHValidateExtantionProtocol {
    
    func isValidObjects(doneBlock: InspectableManyObjectDoneBlockType?) {
        
        var inspectedObjects      = [DPHValidateExtantionProtocol]()
        var wrongValidatorObjects = [DPHValidatorProtocol]()
        
        for textField in self {
            
            textField.isValidData({ (success, inspectedObject, wrongValidatorObject) in
                if !success{
                    if let inspectedObject      = inspectedObject {
                        inspectedObjects.append(inspectedObject)
                    }
                    if let wrongValidatorObject = wrongValidatorObject {
                        wrongValidatorObjects.append(wrongValidatorObject)
                    }
                }
            })
        }
        
        if wrongValidatorObjects.count == 0 {
            if let doneBlock = doneBlock {
                doneBlock(success: true, inspectedObjects: nil, wrongValidatorObjects: nil)
            }
        }else{
            if let doneBlock = doneBlock {
                doneBlock(success: false, inspectedObjects: inspectedObjects, wrongValidatorObjects: wrongValidatorObjects)
            }
        }
        
    }
    
    func isValidObjects() -> Bool {
        
        var wrongValueFound : Bool = false
        for textField in self {
            if !textField.isValidData() {
                wrongValueFound = true
            }
        }
        
        return !wrongValueFound
        
    }
    
}
