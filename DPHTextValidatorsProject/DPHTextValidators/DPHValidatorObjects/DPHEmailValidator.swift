//
//  DPHEmailValidator.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 13.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation

class DPHEmailValidator: DPHValidatorObject{
    
    // MARK: Override
    override func validateValue(value: AnyObject!, inspectedObjectName: String?) -> Bool {
        super.validateValue(value, inspectedObjectName: inspectedObjectName)
        
        if var stringValue = value as? String  {
            stringValue = stringValue.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            self.updateErrorMessage(String().stringByAppendingFormat(NSLocalizedString("message_invalid_email", comment: ""),(self.inspectedObjectName) ?? ""))
            return self.validateEmail(stringValue)
        }else{
            return false
        }
        
    }
    
    // MARK: Helpers
   private  func validateEmail(emailString: String) -> Bool {
        let emailRegex : String! = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let predicate : NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return predicate.evaluateWithObject(emailString)
    }
}