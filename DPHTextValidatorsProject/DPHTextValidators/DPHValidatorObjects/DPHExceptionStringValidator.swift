//
//  DPHExceptionStringValidator.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 19.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation

class DPHExceptionStringValidator: DPHValidatorObject {
    
    var exceptionString : String!
    
    // MARK: Init
    override init() {
        super.init()
    }
    
    init(exceptionString: String!) {
        super.init()
        self.exceptionString = exceptionString
    }
    
    // MARK: Override
    override func validateValue(value: AnyObject!, inspectedObjectName: String?) -> Bool {
        super.validateValue(value, inspectedObjectName: inspectedObjectName)
        assert(self.exceptionString != nil, "exceptionString is empty. Use init(exceptionString: String!)")
        
        if let stringValue = value as? String  {
            if stringValue.rangeOfString(self.exceptionString) != nil {
                self.updateErrorMessage(NSLocalizedString("auth_registration_spaces_pass", comment: ""))
                return false
            }
        }else{
            return false
        }
        return true
    }
}
