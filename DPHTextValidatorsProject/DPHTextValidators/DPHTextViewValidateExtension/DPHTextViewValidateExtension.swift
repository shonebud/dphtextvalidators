//
//  DPHTextViewValidateExtension.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 26.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation
import UIKit

private var validateExtantionAssociationKey: UInt8 = 0

extension UITextView : DPHValidateExtantionProtocol {
    
    private var validateExtantion: DPHValidateExtantion? {
        get {
            return objc_getAssociatedObject(self, &validateExtantionAssociationKey) as? DPHValidateExtantion
        }
        set(newValue) {
            objc_setAssociatedObject(self, &validateExtantionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    //MARK: Validating
    func valueForValidating() -> AnyObject! {
        return self.text
    }
    
    func inspectedObjectName() -> String? {
        return nil
    }
    
    func addValidators(validators: [DPHValidatorProtocol]) {
        if self.validateExtantion == nil {
            self.validateExtantion = DPHValidateExtantion(owner: self)
        }
        self.validateExtantion?.addValidators(validators)
    }
    
    func isValidData(inspectableDoneBlock: InspectableObjectDoneBlockType?) {
        self.validateExtantion?.isValidData({ (success, inspectedObjects, wrongValidatorObjects) in
            self.validatedWithResult(success)
            
            if let inspectableDoneBlock = inspectableDoneBlock {
                inspectableDoneBlock(success: success, inspectedObjects: inspectedObjects, wrongValidatorObjects: wrongValidatorObjects)
            }
        })
    }
    
    func isValidData() -> Bool {
        var success : Bool = true
        if self.validateExtantion != nil {
            success = self.validateExtantion!.isValidData()
        }
        self.validatedWithResult(success)
        return success
    }
    
    func showErrorMessage(flag: Bool) {
        self.validateExtantion?.showErrorMessage = flag
    }
    
    func displayErrorMesage(message: String) {
        self.validateExtantion?.displayErrorMesage(message)
    }
    
    //MARK: Helpers
    func validatedWithResult(success: Bool) {
        
    }
    
}