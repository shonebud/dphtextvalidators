//
//  DPHInputTextField.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 29.04.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation
import UIKit

class DPHInputTextField: UITextField, UITextFieldDelegate {
    
    enum InputTextFieldStateType : Int {
        case DefaultState = 0
        case LoadingState
        case VerifiedState
        case ErrorState
    }
    
    private var contentView : UIView?
    private var moveWithKeyboardAlloved : Bool = false
    
    var activityIndicator : UIActivityIndicatorView?
    var placeholderDefaultTextColor : UIColor?
    var contextImageWrapeView : UIView?
    var moveWithKeyboardDelegateDoneBlock : ((moveOffset : CGFloat) -> Void)?
    weak var outDelegate : UITextFieldDelegate?
    
    override var placeholder: String? {
        didSet {
            configureTextPlaceholder()
        }
    }
    
    lazy var underlineImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRectZero)
        self.addSubview(imageView)
        
        return imageView
    }()
    
    lazy var rightOptionButton: UIButton = {
        let button = UIButton(frame: CGRectZero)
        button.addTarget(self, action: #selector(rightOptionButtonAction), forControlEvents: UIControlEvents.TouchUpInside)
        
        return button
    }()
    
    var stateType : InputTextFieldStateType = .DefaultState {
        didSet {
            self.configureUnderlineImage()
            self.configureTextPlaceholder()
            self.activityIndicator?.stopAnimating()
            
            switch stateType {
            case .LoadingState:
                self.activityIndicator?.startAnimating()
                self.hideRightIcon()
                
            case .ErrorState:
                if (self.text?.characters.count > 0) {
                    self.showRightOptionButton()
                }else{
                    self.hideRightIcon()
                }
                
            case .VerifiedState:
                self.showVerifiedButton()
                
            case .DefaultState:
                self.hideRightIcon()
            }
        }
    }
    
    // MARK: Context image
    @IBInspectable var textOffsetValue: CGPoint = CGPoint(x: 0, y: 0) {
        didSet {
            if (contextImage != nil) {
                self.configureContextImage()
            }
        }
    }
    
    @IBInspectable var contextImage: UIImage? {
        didSet {
            self.configureContextImage()
        }
    }
    
    // MARK: Clear button image
    @IBInspectable var clearButtonImage: UIImage? {
        didSet {
            self.configureRightOptionButton()
        }
    }
    
    // MARK: Clear button image
    @IBInspectable var verifiedButtonImage: UIImage? {
        didSet {
            self.configureRightOptionButton()
        }
    }
    
    // MARK: Underline view
    @IBInspectable var underlineImage: UIImage = UIImage(named: "input-Normal")! {
        didSet {
            configureUnderlineImage()
        }
    }
    
    @IBInspectable var errorUnderlineImage: UIImage = UIImage(named: "input-Error")! {
        didSet {
            configureUnderlineImage()
        }
    }
    
    @IBInspectable var verifiedUnderlineImage: UIImage = UIImage(named: "input-Verified")! {
        didSet {
            configureUnderlineImage()
        }
    }
    
    // MARK: Placeholder
    @IBInspectable var placeholderTextColor: UIColor! = UIColor.lightTextColor() {
        didSet {
            self.configureTextPlaceholder()
        }
    }
    
    @IBInspectable var errorPlaceholderTextColor: UIColor! = UIColor.lightTextColor() {
        didSet {
            self.configureTextPlaceholder()
        }
    }
    
    @IBInspectable var placeholderTextSize: CGFloat = 14 {
        didSet {
            self.configureTextPlaceholder()
        }
    }
    
    
    // MARK: Overvie
    deinit {
        self.removeKeyboardObserving()
        self.delegate = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.outDelegate = self.delegate
        self.delegate = self
        
        self.contentView = self.superview
        
        self.configureTextPlaceholder()
        self.configureActivityIndicatorView()
        configureUnderlineImage()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let indicatorWidth = CGRectGetWidth((self.activityIndicator?.frame)!)
        self.activityIndicator?.frame = CGRectMake(CGRectGetWidth(self.frame) - indicatorWidth, (CGRectGetHeight(self.frame) - indicatorWidth) / 2, indicatorWidth, indicatorWidth)
        
        let underlineHeight = underlineImageView.image?.size.height ?? 1
        underlineImageView.frame = CGRect(x: 0, y: frame.height - underlineHeight, width: frame.width, height: underlineHeight)
    }
    
    //MSRK: Validation
    
    override func validatedWithResult(success: Bool) {
        if !success {
            self.stateType = .ErrorState
        }else{
            self.stateType = .VerifiedState
        }
 
    }
    
    // MARK: Setup
    private func configureContextImage() {
        if (contextImage != nil) {
            let contextImageView = UIImageView(image: contextImage)
            contextImageWrapeView = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(contextImageView.frame) + textOffsetValue.x, CGRectGetHeight(self.frame)))
            
            var contextImageViewFrame = contextImageView.frame
            contextImageViewFrame.origin.y = (CGRectGetHeight(self.frame) - CGRectGetHeight(contextImageView.frame)) / 2;
            contextImageView.frame = contextImageViewFrame
            
            contextImageWrapeView!.addSubview(contextImageView)
            
            self.leftViewMode = UITextFieldViewMode.Always
            self.leftView = contextImageWrapeView
        }
    }
    
    private func configureRightOptionButton() {
        guard let rightOptionButtonImage = (self.stateType == .VerifiedState) ? self.verifiedButtonImage : self.clearButtonImage else { return }
        
        rightOptionButton.frame = CGRectMake(0, 0, rightOptionButtonImage.size.width, rightOptionButtonImage.size.height)
        rightOptionButton.setImage(rightOptionButtonImage, forState: UIControlState.Normal)
        
        rightView = rightOptionButton
    }
    
    func hideRightIcon() {
        self.rightViewMode = UITextFieldViewMode.Never
        self.rightView = nil
    }
    
    func showRightOptionButton() {
        self.configureRightOptionButton()
        self.rightViewMode = UITextFieldViewMode.Always
    }
    
    func showVerifiedButton() {
        self.configureRightOptionButton()
        self.rightViewMode = UITextFieldViewMode.Always
    }

    
    private func configureUnderlineImage() {
        switch stateType {
        case .VerifiedState: underlineImageView.image = verifiedUnderlineImage
        case .ErrorState:    underlineImageView.image = errorUnderlineImage
        default:             underlineImageView.image = underlineImage
        }
        bringSubviewToFront(underlineImageView)
    }
    
    private func configureActivityIndicatorView() {
        if self.activityIndicator == nil {
            self.activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        }
        
        self.addSubview(self.activityIndicator!)
    }
    
    private func configureTextPlaceholder () {
        if (self.placeholder != nil) {
            let textColor = (stateType == .ErrorState) ? errorPlaceholderTextColor : placeholderTextColor
            self.attributedPlaceholder = NSAttributedString(string:NSLocalizedString(self.placeholder!, comment: ""),
                                                            attributes: [NSForegroundColorAttributeName: textColor,
                                                                NSFontAttributeName: UIFont.systemFontOfSize(placeholderTextSize)])
        }
    }
    
    // MARK: Actions
    func rightOptionButtonAction() {
        if  self.stateType != .VerifiedState {
            self.text = ""
            self.stateType = .DefaultState
        }
    }
    
    // MARK: Helpers
    func addKeyboardObserving(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardDidHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func removeKeyboardObserving(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func centeredTextFields(keyboardHeight: CGFloat, containerView: UIView!){
        
        var rootView : UIView! = self.superview
        while rootView?.superview != nil {
            rootView = rootView!.superview
        }
        
        let freeSpace = CGRectGetHeight(rootView.frame) - keyboardHeight
        let convertedSelfPosition : CGPoint = containerView.convertPoint(self.frame.origin, toView: rootView)
        let textFieldsMinY : CGFloat = (containerView?.convertPoint(self.frame.origin, toView: rootView).y)!
        let textFieldsMaxY : CGFloat = convertedSelfPosition.y + CGRectGetHeight(self.frame)
        let topOffsetTargetValue : CGFloat = (freeSpace - CGFloat(textFieldsMaxY - textFieldsMinY)) / 2.0
        let missingOffset = topOffsetTargetValue - CGFloat(textFieldsMinY)
        
        if let containerView = containerView {
            self.moveContainerView(containerView, offset: containerView.transform.ty + missingOffset)
        }

    }
    
    func moveContainerView(containerView: UIView, offset: CGFloat) {
            containerView.transform = CGAffineTransformMakeTranslation(0, offset)
        if let doneBlock = self.moveWithKeyboardDelegateDoneBlock {
            doneBlock(moveOffset: offset)
        }
       
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.centeredTextFields(keyboardSize.height, containerView: self.contentView)
        }
    }
    
    func keyboardDidHide(notification: NSNotification) {
        self.resetContainerViewOffset()
        
    }
    
    func resetContainerViewOffset(){
        if let containerView = self.contentView {
            UIView.animateWithDuration(0.3, animations: {
                self.moveContainerView(containerView, offset: 0)
                }, completion: nil)
        }
    }
    
    // MARK: UITextFieldDelegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.stateType = .DefaultState
        
        if let returtValue = self.outDelegate?.textField?(textField, shouldChangeCharactersInRange: range, replacementString: string) {
            return returtValue
        }
        return true;
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        var returtValue = true
        
        if let flag = self.outDelegate?.textFieldShouldBeginEditing?(textField) {
            returtValue = flag
        }
        if returtValue {
            if self.moveWithKeyboardAlloved {
                self.addKeyboardObserving()
            }else{
                UIView.animateWithDuration(0.3, animations: {
                    self.contentView?.transform = CGAffineTransformMakeTranslation(0, 0)
                })
            }
        }
        return returtValue
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.outDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if let returtValue = self.outDelegate?.textFieldShouldEndEditing?(textField) {
            return returtValue
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.outDelegate?.textFieldDidEndEditing?(textField)
        self.removeKeyboardObserving()
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        if let returtValue = self.outDelegate?.textFieldShouldClear?(textField) {
            return returtValue
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if let returtValue = self.outDelegate?.textFieldShouldReturn?(textField) {
            return returtValue
        }
        return true
    }
    
    // MARK: Public
    
    func allowMoveWithKeyboardShowing(allow: Bool, doneBlock: ((offset: CGFloat) -> Void)?){
        self.moveWithKeyboardAlloved = allow
        self.moveWithKeyboardDelegateDoneBlock = doneBlock
        if !allow {
            self.removeKeyboardObserving()
            self.resetContainerViewOffset()
        }else if self.editing {
            self.addKeyboardObserving()
        }
    }
    
}