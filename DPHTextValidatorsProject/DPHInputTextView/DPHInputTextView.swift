//
//  DPHInputTextView.swift
//  Depositphotos
//
//  Created by Vitalii Todorovych on 26.05.16.
//  Copyright © 2016 Depositphotos Inc. All rights reserved.
//

import Foundation
import UIKit

class DPHInputTextView: UITextView, UITextViewDelegate {
    
    enum InputTextFieldStateType : Int {
        case DefaultState = 0
        case LoadingState
        case VerifiedState
        case ErrorState
    }
    
    let defaultContentOffset = UIEdgeInsetsMake(8, -4, 0, 0)
    var containerView : UIView! = UIView(frame: CGRectZero)
    
    var placeholderDefaultTextColor : UIColor?
    var underlineImageView : UIImageView?
    var rightOptionButton : UIButton?
    var placeholderLablel : UILabel?
    weak var outDelegate : UITextViewDelegate?
    
    private var contentView : UIView?
    private var moveWithKeyboardAlloved : Bool = false
    
    var moveWithKeyboardDelegateDoneBlock : ((moveOffset : CGFloat) -> Void)?
    
    var stateType : InputTextFieldStateType = .DefaultState {
        didSet {
            self.configureUnderlineImage()
            self.configureTextPlaceholder()
            
            switch stateType {
            case .LoadingState:
                self.showRightOptionButton(false)
                
            case .ErrorState:
                if (self.text?.characters.count > 0) {
                    self.showRightOptionButton(true)
                }else{
                    self.showRightOptionButton(false)
                }
                
            case .VerifiedState:
                self.showRightOptionButton(true)
                
            case .DefaultState:
                self.showRightOptionButton(false)
            }
        }
    }
    
    // MARK: Clear button image
    @IBInspectable var clearButtonImage: UIImage? {
        didSet {
            self.configureRightOptionButton()
        }
    }
    
    // MARK: Clear button image
    @IBInspectable var verifiedButtonImage: UIImage? {
        didSet {
            self.configureRightOptionButton()
        }
    }
    
    // MARK: Underline view
    @IBInspectable var underlineImage: UIImage? {
        didSet {
            self.configureUnderlineImage()
        }
    }
    
    @IBInspectable var errorUnderlineImage: UIImage? {
        didSet {
            if (errorUnderlineImage != nil) {
                self.configureUnderlineImage()
            }
        }
    }
    
    @IBInspectable var verifiedUnderlineImage: UIImage? {
        didSet {
            if (verifiedUnderlineImage != nil) {
                self.configureUnderlineImage()
            }
        }
    }
    
    // MARK: Placeholder
    @IBInspectable var placeholder: String? {
        didSet {
            self.configureTextPlaceholder()
        }
    }
    
    @IBInspectable var placeholderTextColor: UIColor! = UIColor.lightTextColor() {
        didSet {
            if (self.placeholder != nil) {
                self.configureTextPlaceholder()
            }
        }
    }
    
    @IBInspectable var errorPlaceholderTextColor: UIColor! = UIColor.lightTextColor() {
        didSet {
            if (self.placeholder != nil) {
                self.configureTextPlaceholder()
            }
        }
    }
    
    @IBInspectable var placeholderTextSize: CGFloat = 14 {
        didSet {
            if (self.placeholder != nil) {
                self.configureTextPlaceholder()
            }
        }
    }
    
    // MARK: Overvie
    deinit {
        self.removeKeyboardObserving()
        self.delegate = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.outDelegate = self.delegate
        self.delegate = self
        
        self.textContainerInset = defaultContentOffset
        
        self.containerView.userInteractionEnabled = false

        self.showsVerticalScrollIndicator = false
        self.addSubview(containerView)
        self.contentView = self.superview
        
        self.configureTextPlaceholder()
    }
    
    override func inspectedObjectName() -> String? {
        return self.placeholder
    }
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        
        if let rightOptionButton = self.rightOptionButton{
            let pointWithOffset = CGPointMake(point.x, point.y - self.contentOffset.y)
            if CGRectContainsPoint(self.convertRect(rightOptionButton.frame, toView: self), pointWithOffset) {
                self.rightOptionButtonAction()
                return true
            }
        }
        return CGRectContainsPoint(self.bounds, point)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let placeholderLablel = self.placeholderLablel {
            var frame = placeholderLablel.frame
            frame.origin.x = 0
            frame.origin.y = 8
            placeholderLablel.frame = frame
        }
        
        containerView.frame = CGRectMake(0, containerView.frame.origin.y, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))
        
        if let rightOptionButton = self.rightOptionButton {
            var frame = rightOptionButton.frame
            frame.origin.x = CGRectGetWidth(containerView.frame) - CGRectGetWidth(rightOptionButton.frame)
            frame.origin.y = CGRectGetHeight(containerView.frame) - CGRectGetHeight(rightOptionButton.frame) - 10
            rightOptionButton.frame = frame
        }
        
        if let underlineImageView = self.underlineImageView {
            underlineImageView.frame = CGRectMake(0, CGRectGetHeight(containerView.frame) - CGRectGetHeight(underlineImageView.frame), CGRectGetWidth(containerView.frame), CGRectGetHeight(underlineImageView.frame))
        }
    }
    
    //MSRK: Validation
    
    override func validatedWithResult(success: Bool) {
        if !success {
            self.stateType = .ErrorState
        }else{
            self.stateType = .VerifiedState
        }
    }
    
    // MARK: Setup
    private func configureRightOptionButton() {
        if (rightOptionButton == nil) {
            rightOptionButton = UIButton.init(frame: CGRectMake(0, 0, 0, 0))
            rightOptionButton?.addTarget(self, action: #selector(rightOptionButtonAction), forControlEvents: UIControlEvents.TouchUpInside)
            self.rightOptionButton?.hidden = true
        }
        let rightOptionButtonImage : UIImage? = (self.stateType == .VerifiedState) ? self.verifiedButtonImage : self.clearButtonImage;
        rightOptionButton!.frame = CGRectMake(CGRectGetWidth(containerView.frame) - (rightOptionButtonImage?.size.width)!, CGRectGetHeight(containerView.frame) - (rightOptionButtonImage?.size.height)! - 10, (rightOptionButtonImage?.size.width)!, (rightOptionButtonImage?.size.height)!)
        rightOptionButton!.setImage(rightOptionButtonImage, forState: UIControlState.Normal)
        containerView.addSubview(rightOptionButton!)
    }
    
    private func showRightOptionButton(flag : Bool) {
        self.configureRightOptionButton()
        if let rightOptionButton = self.rightOptionButton {
            rightOptionButton.hidden = !flag
            self.textContainerInset = UIEdgeInsetsMake(defaultContentOffset.top, defaultContentOffset.left, defaultContentOffset.bottom, flag ? CGRectGetWidth(rightOptionButton.frame) : defaultContentOffset.right);
        }
    }
    
    private func configureUnderlineImage() {
        var image : UIImage? = underlineImage
        switch stateType {
        case .ErrorState:
            if (errorUnderlineImage != nil) {
                image = errorUnderlineImage
            }
        case .VerifiedState:
            if (verifiedUnderlineImage != nil) {
                image = verifiedUnderlineImage
            }
        default: break
        }
        if let image = image {
            if underlineImageView == nil {
                underlineImageView = UIImageView(frame: CGRectMake(0, 0, 0, 0))
            }
            underlineImageView!.image = image
            underlineImageView!.frame = CGRectMake(0, CGRectGetHeight(containerView.frame) - image.size.height, CGRectGetWidth(containerView.frame), image.size.height)
            self.containerView.addSubview(underlineImageView!)
        }
    }
    
    private func configureTextPlaceholder () {
        if (self.placeholder != nil) {
            if self.placeholderLablel == nil {
                self.placeholderLablel = UILabel.init(frame: CGRectMake(0, 0, 0, 0))
            }
            let textColor = (stateType == .ErrorState) ? errorPlaceholderTextColor : placeholderTextColor
            self.placeholderLablel?.attributedText = NSAttributedString(string:NSLocalizedString(self.placeholder!, comment: ""),
                                                            attributes: [NSForegroundColorAttributeName: textColor,
                                                                NSFontAttributeName: UIFont.systemFontOfSize(placeholderTextSize)])
            self.placeholderLablel?.sizeToFit()
            self.addSubview(self.placeholderLablel!)
            self.placeholderLablel?.hidden = !(self.text.characters.count == 0)
        }
    }
    
    // MARK: Actions
    func rightOptionButtonAction() {
        if  self.stateType != .VerifiedState {
            self.text = ""
            self.stateType = .DefaultState
        }
    }
    
    // MARK: Helpers
    private func addKeyboardObserving(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardDidHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func removeKeyboardObserving(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    private func centeredTextFields(keyboardHeight: CGFloat, containerView: UIView!){
        
        var rootView : UIView! = self.superview
        while rootView?.superview != nil {
            rootView = rootView!.superview
        }
        
        let freeSpace = CGRectGetHeight(rootView.frame) - keyboardHeight
        let convertedSelfPosition : CGPoint = containerView.convertPoint(self.frame.origin, toView: rootView)
        let textFieldsMinY : CGFloat = (containerView?.convertPoint(self.frame.origin, toView: rootView).y)!
        let textFieldsMaxY : CGFloat = convertedSelfPosition.y + CGRectGetHeight(self.frame)
        let topOffsetTargetValue : CGFloat = (freeSpace - CGFloat(textFieldsMaxY - textFieldsMinY)) / 2.0
        let missingOffset = topOffsetTargetValue - CGFloat(textFieldsMinY)
        
        if let containerView = containerView {
            self.moveContainerView(containerView, offset: containerView.transform.ty + missingOffset)
        }
        
    }
    
    private func moveContainerView(containerView: UIView, offset: CGFloat) {
        containerView.transform = CGAffineTransformMakeTranslation(0, offset)
        if let doneBlock = self.moveWithKeyboardDelegateDoneBlock {
            doneBlock(moveOffset: offset)
        }
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.centeredTextFields(keyboardSize.height, containerView: self.contentView)
        }
    }
    
    func keyboardDidHide(notification: NSNotification) {
        self.resetContainerViewOffset()
    }
    
    func resetContainerViewOffset(){
        if let containerView = self.contentView {
            UIView.animateWithDuration(0.3, animations: {
                self.moveContainerView(containerView, offset: 0)
                }, completion: nil)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var frame = self.containerView.frame
        frame.origin.y = scrollView.contentOffset.y
        self.containerView.frame = frame
    }
    
    // MARK: UITextViewDelegate
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        if let returtValue = self.outDelegate?.textViewShouldEndEditing?(textView){
            return returtValue
        }
        return true
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        var returtValue = true
        if let flag = self.outDelegate?.textViewShouldBeginEditing?(textView){
            returtValue = flag
        }
        if returtValue {
            if self.moveWithKeyboardAlloved {
                self.addKeyboardObserving()
            }else{
                UIView.animateWithDuration(0.3, animations: {
                    self.contentView?.transform = CGAffineTransformMakeTranslation(0, 0)
                })
            }
        }
        
        return returtValue
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.outDelegate?.textViewDidBeginEditing?(textView)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        self.outDelegate?.textViewDidEndEditing?(textView)
         self.removeKeyboardObserving()
    }
    
    func textViewDidChange(textView: UITextView) {
        self.stateType = .DefaultState
        self.outDelegate?.textViewDidChange?(textView)
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {

        if let returtValue = self.outDelegate?.textView?(textView, shouldChangeTextInRange: range, replacementText: text){
            return returtValue
        }
        return true
    }

    func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        self.stateType = .DefaultState
        
        if let returtValue = self.outDelegate?.textView?(textView, shouldInteractWithURL: URL, inRange: characterRange) {
            return returtValue
        }
        return true;
    }
    
    func textViewDidChangeSelection(textView: UITextView) {
        self.outDelegate?.textViewDidChangeSelection?(textView)
    }
    
    // MARK: Public
    func allowMoveWithKeyboardShowing(allow: Bool, doneBlock: ((offset: CGFloat) -> Void)?){
        self.moveWithKeyboardAlloved = allow
        self.moveWithKeyboardDelegateDoneBlock = doneBlock
        if !allow {
            self.removeKeyboardObserving()
            self.resetContainerViewOffset()
        }else if self.isFirstResponder() {
            self.addKeyboardObserving()
        }
    }
    

}
